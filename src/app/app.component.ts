import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-task';
  header = [
    {name: 'OUR BEERS'},
    {name: 'PACKAGES'},
    {name: 'RESERVATION'},
    {name: 'GALLERY'},
    {name: 'ABOUT US'},
    {name: 'CONTACT US'},
    {name: 'ORDER ONLINE'}
  ];
}
